from random import randint

user_name = input("Hi! What is your name? ")

yes_or_no_answers = ['Yes', 'yes', 'No', 'no']
year_earlier_or_later_answer = ['Earlier', 'earlier', 'Later', 'later']

start_year = 1924
end_year = 2004

for num in range(5):
    month = randint(1,12)
    year = randint(start_year,end_year)
    guess_birthday_str = str(month) + '/' + str(year)
    yes_or_no_answer = input("Guess number " + str(num + 1) + ": " + user_name + " were you born in " + guess_birthday_str + "? Yes or No ")
    if yes_or_no_answer in yes_or_no_answers:
        if yes_or_no_answer == 'Yes' or yes_or_no_answer == 'yes':
            print("I knew it!")
            break
        elif yes_or_no_answer == 'No' or yes_or_no_answer == 'no' and num < 4:

            # Need to get user input to determine whether year is earlier or later

            year_earlier_or_later = input("Is the year earlier or later? ")
            if year_earlier_or_later in year_earlier_or_later_answer:
                if year_earlier_or_later == "earlier":
                    end_year = year - 1
                else:
                    start_year = year + 1
            else:
                print("Not an acceptable answer, please try again later.")
                break
        else:
            print("I have other things to do. Good bye.")
    else:
        print("Not an acceptable answer, please try again later.")
        break
